import sys
from exception import HttpException

py_version = sys.version_info[0]

if py_version >= 3:
    #If you are running python 3 or above
    from urllib.request import urlopen
    from urllib.error import HTTPError
    from urllib.parse import urlencode
else:
    #If you are running python 2
    raise Exception('Verify if you have python 3 installed!')

BASE_URL = "https://www.binance.com/api/"

def call_api(resource, data=None, base_url=None):
    base_url =  BASE_URL if base_url is None else base_url
    try:
        payload = None if data is None else urlencode(data)
        response = urlopen(base_url + resource, payload).read()
        return response_handler(response)
    except HTTPError as error:
        raise HttpException(response_handler(error.read()), error.code)

def response_handler(response):
        return response.decode('utf-8')
