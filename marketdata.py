from core import call_api
import json

def get(api_code=None):

    """Get network statistics from binance exchange
    
    :param str api_code: Binance API code (optional)
    :return: an instance of :class:`Stats` class
    """
    
    resource = 'v3/exchangeInfo'
    if api_code is not None:
        resource += '&signature=' + api_code
    response = call_api(resource)
    json_response = json.loads(response)
    return Stats(json_response)

def get_price(symbol,api_code=None):

    """Get a coin average price from binance exchange
    
    :param str api_code: Binance API code (optional)
    :param str symbol: Binance symbol code representing a coin
    :return: an instance of :class:`Price` class
    """

    if symbol is not str:
        symbol = str(symbol)
    
    resource = 'v3/avgPrice?symbol=' + symbol
    
    if api_code is not None:
        resource += '&signature=' + api_code

    response = call_api(resource)
    json_response = json.loads(response)
    return Price(json_response)

def best_price(symbol,api_code=None):

    """Get a coin best price from binance exchange
    
    :param str api_code: Binance API code (optional)
    :param str symbol: Binance symbol code representing a coin
    :return: an instance of :class:`Price` class
    """

    if symbol is not str:
        symbol = str(symbol)
    
    resource = 'v3/ticker/bookTicker?symbol=' + symbol
    
    if api_code is not None:
        resource += '&signature=' + api_code

    response = call_api(resource)
    json_response = json.loads(response)
    return BestPrice(json_response)

def price_24(symbol,api_code=None):

    """Get a coin historic price from last 24 hours from binance exchange
    
    :param str api_code: Binance API code (optional)
    :param str symbol: Binance symbol code representing a coin
    :return: an instance of :class:`Price24` class
    """

    if symbol is not str:
        symbol = str(symbol)
    
    resource = 'v3/ticker/24hr?symbol=' + symbol
    
    if api_code is not None:
        resource += '&signature=' + api_code

    response = call_api(resource)
    json_response = json.loads(response)
    return Price24(json_response)

def trade(symbol,limit=None, api_code=None):

    """Get a coin recent trades (default is the last 500) from binance exchange
    
    :param str api_code: Binance API code (optional)
    :param str symbol: Binance symbol code representing a coin
    :return: an instance of :class:`Trades` class
    """

    if symbol is not str:
        symbol = str(symbol)
    
    resource = 'v3/trades?symbol=' + symbol
    
    if limit is not None and limit <= 1000:
        limit = str(limit)
        resource += '&limit=' + limit
 
    if api_code is not None:
        resource += '&signature=' + api_code

    response = call_api(resource)
    json_response = json.loads(response)
    return [Trades(item) for item in json_response]

def candlestickinfo(symbol, interval, start=None, end=None, limit=None, api_code=None):
    """Get a coin recent trades (default is the last 500) from binance exchange
    
    :param str api_code: Binance API code (optional)
    :param str symbol: Binance symbol code representing a coin
    :return: an instance of :class:`Trades` class
    """
    pass

    if symbol is not str:
        symbol = str(symbol)
    
    if interval is not str:
        interval = str(interval)

    resource = 'v3/klines?symbol=' + symbol + '&interval=' + interval
    
    if limit is not None and limit <= 1000:
        limit = str(limit)
        resource += '&limit=' + limit
 
    if api_code is not None:
        resource += '&signature=' + api_code

    response = call_api(resource)
    json_response = json.loads(response)
    return [Trades(item) for item in json_response]

class Trades:
    def __init__(self,t):
        self.id = t['id'] #returns a list type of objectst['id']
        self.price = t['price']
        self.quantity = t['qty']
        self.quote_quantity = t['quoteQty']
        self.time = t['time']
        self.isbuyermaker = t['isBuyerMaker']
        self.isbestmatch = t['isBestMatch']

class Price24:
    def __init__(self,p24):
        self.symbol = p24['symbol']
        self.priceChange = p24['priceChange']
        self.priceChangePercent = p24['priceChangePercent']
        self.weightedAvgPrice = p24['weightedAvgPrice']
        self.prevClosePrice = p24['prevClosePrice']
        self.lastPrice = ['lastPrice']
        self.lastQuantity = p24['lastQty']
        self.bidPrice = p24['bidPrice']
        self.askPrice = p24['askPrice']
        self.openPrice = p24['openPrice']
        self.highPrice = p24['highPrice']
        self.lowPrice = p24['lowPrice']
        self.volume = p24['volume']
        self.quoteVolume = p24['quoteVolume']
        self.openTime = p24['openTime']
        self.closeTime = p24['closeTime']
        self.firstId = p24['firstId']
        self.lastId = p24['lastId']
        self.count = p24['count']
    
class BestPrice:
    def __init__(self,bp):
        self.bid_price = bp['bidPrice']
        self.asked_price = bp['askPrice']
        self.bid_quantity = bp['bidQty']
        self.asked_quantity = bp['askQty']

class Price:
    def __init__(self,p):
        self.price = p['price']

class Stats:
    def __init__(self, s):
        self.timezone = s['timezone']
        self.servertime = s['serverTime']
        self.ratelimits = s['rateLimits']
        self.symbols = s['symbols']
        self.info = [Info(info) for info in s['symbols']] #returns a list type of objects

class Info:
    def __init__(self,i):
        self.symbol = i['symbol']
        self.status = i['status']
        self.baseasset = i['baseAsset']
        self.baseassetprecision = i['baseAssetPrecision']
        self.quoteasset = i['quoteAsset']
        self.quoteprecision = i['quotePrecision']
        self.ordertypes = i['orderTypes']
        self.icebergallowed = i['icebergAllowed']
        self.ocoallowed = i['ocoAllowed']
        self.isspottradingallowed = i['isSpotTradingAllowed']
        self.ismargintradingallowed = i['isMarginTradingAllowed']
        self.filters = i['filters']

for k in get().info:
    print("{}, {}, {}".format(k.symbol, k.status, k.baseasset)) 
print(get_price('BTCUSDT').price)
print(best_price('BTCUSDT').bid_price)
print(price_24('BTCUSDT').priceChange)
for i in trade('BTCUSDT'):
    print(i.id)